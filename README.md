# Minesweep solver

Minesweeper "solver" on freeminesweeper.org/minecore.html. Solves sizes 15x30.
Opens squares and places flags only if it is certain of those
Currently doesn't calculate any probabilities and opens random squares if it gets stuck.

Uses mss to take screenshots and opencv2 (mainly template matching) to process images.
