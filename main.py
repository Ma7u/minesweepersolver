# pyautogui, control mouse
# opencv, process image
# mss, take screenshots
# numpy, make screenshot to numpy array

import pyautogui
import cv2
import mss
import mss.tools
import numpy
import os
import random


def get_pics():
    pyautogui.moveTo(500, 500)
    with mss.mss() as sct:
        monitor = {'top': 174, 'left': 21, 'width': 496, 'height': 255}

        img = numpy.array(sct.grab(monitor))

        square_pics = []
        for height in range(0, 16):
            square_pics1 = []
            for width in range(0, 31):
                square_pics1.append(img[height*16:height*16+16, width*16:width*16+16])
            square_pics.append(square_pics1)
    return square_pics


def get_numbers(square_pics):
    threshhold = 0.98
    square_numbers = []
    for i in range(0, 16):
        square_numbers1 = []
        for j in range(0, 31):

            pic = cv2.cvtColor(square_pics[i][j], cv2.COLOR_BGRA2GRAY)
            for file in os.listdir('templates'):
                template = cv2.imread('templates/'+file, 0)
                res = cv2.matchTemplate(pic, template, cv2.TM_CCOEFF_NORMED)

                if len(numpy.where(res >= threshhold)[0]) > 0:
                    # Found the right template
                    number = int(file[:-4])
                    square_numbers1.append(number)
                    break
        square_numbers.append(square_numbers1)
    return square_numbers


def get_locations():
    square_loc = []
    for height in range(0, 16):
        square_loc1 = []
        for width in range(0, 31):
            square_loc1.append((16*width+28, 16*height+183))
        square_loc.append(square_loc1)
    return square_loc


def start_game():
    first_squares = [(157, 230), (380, 229), (157, 373), (381, 373)]
    for i in range(0, 4):
        pyautogui.moveTo(first_squares[i])
        pyautogui.click()


def check_board(square_locations, square_numbers):
    guess_next_one = True
    # Loop all squares
    for i in range(0, 16):
        for j in range(0, 31):

            blanks = []
            flags = []

            number = square_numbers[i][j]
            if number == -3:  # Game is over
                return True
            if number in [1, 2, 3, 4, 5, 6]:
                # Loop adjacent squares
                for row in range(-1, 2):
                    for column in range(-1, 2):
                        try:
                            # Checks that the rows and columns don't end
                            if i + row < 0 or j + column < 0 \
                                    or i + row > 15 or j + column > 30:
                                raise IndexError

                            if square_numbers[i+row][j+column] == -1:
                                blanks.append([
                                    square_locations[i+row][j+column],
                                    [i+row, j+column]])

                            elif square_numbers[i+row][j+column] == -2:
                                flags.append(square_locations[i+row][j+column])
                        except IndexError:
                            # There's no more squares
                            pass

                if len(blanks) + len(flags) == number and len(flags) != number:
                    guess_next_one = False
                    # All blanks are mines
                    for mine_loc in blanks:
                        add_flag(mine_loc[0])
                        square_numbers[mine_loc[1][0]][mine_loc[1][1]] = -2

                elif len(flags) == number and len(blanks) != 0:
                    guess_next_one = False
                    # All blanks are numbers
                    # Clicks the current square
                    #   which opens all the adjacent ones
                    open_square(square_locations[i][j])

    while guess_next_one:
        i = random.randint(0, 15)
        j = random.randint(0, 30)
        print(i, j)
        number = square_numbers[i][j]
        if number == -1:
            open_square(square_locations[i][j])
            guess_next_one = False

    return False


def add_flag(loc):
    pyautogui.moveTo(loc)
    pyautogui.rightClick()


def open_square(loc):
    pyautogui.moveTo(loc)
    pyautogui.click()


def restart_game(loc):
    pyautogui.moveTo(loc)
    pyautogui.click()


def main():
    restart_at_fail = True
    while restart_at_fail:
        restart_game((269, 154))
        square_locations = get_locations()
        start_game()

        game_is_over = False
        while not game_is_over:
            square_pics = get_pics()
            square_numbers = get_numbers(square_pics)
            game_is_over = check_board(square_locations, square_numbers)


main()
